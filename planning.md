* make a virtual environment
    'python -m venv ./.venv'
    -activate
        'source .venv/bin/activate'
* intall django
    'pip install django'
* create a django project
    'django-admin startproject book_reviews .'
* create an app
    'python manage.py startapp reviews'
* make migrations
    `python manage.py makemigrations`
* migrate
    `python manage.py migrate`
* Start Django server
    `python manage.py runserver`
* [ ] Make some templates
* [ ]
    'python manage.py makemigrations

    'python manage.py runserver'
    'python manage.py migrate'